PFE project.
================


Installation
------------
```bash
composer install
```

```bash
php bin/console assets:install --symlink web
```
```bash
php bin/console fos:js-routing:dump
```
```bash
php bin/console bazinga:js-translation:dump --env=prod
```
```bash
php bin/console assetic:dump --env=prod
```


Create & promote user
------------

```bash
php bin/console fos:user:create username email@email.com password
```

```bash
php bin/console fos:user:promote username ROLE_SUPER_ADMIN
```