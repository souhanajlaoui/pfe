// create our webrtc connection
var webrtc = new SimpleWebRTC({
    localVideoEl: 'localVideo',
    remoteVideosEl: '',
    autoRequestMedia: true,
    debug: false,
    detectSpeakingEvents: true,
    nick: {
        name: currentUserName,
        type: currentUserType
    }
});

webrtc.on('readyToCall', function () {
    if (room)
        webrtc.joinRoom(room);
});

function showVolume(el, volume) {
    if (!el)
        return;
    if (volume < -45) { // vary between -45 and -20
        el.style.height = '0px';
    } else if (volume > -20) {
        el.style.height = '100%';
    } else {
        el.style.height = '' + Math.floor((volume + 100) * 100 / 25 - 220) + '%';
    }
}
webrtc.on('channelMessage', function (peer, label, data) {

    if (data.type == 'volume') {
        showVolume(document.getElementById('volume_' + peer.id), data.volume);
    }

    if (data.type == 'spreadsheet') {
        $('#spreadsheet').prepend('<div class="spreadsheet-message">' + peer.nick.name + " : " + data.payload + '</div>');
    }
});

$("#spreadsheet-message").keyup(function (e) {
    var code = e.which;
    if (code == 13) {
        sendMessage();
    }
});

function sendMessage() {
    var element = $('#spreadsheet-message');
    var value = element.val();
    element.val('');
    if (value !== "") {
        webrtc.sendDirectlyToAll('mySuperChannel', 'spreadsheet', value);
        $('#spreadsheet').prepend('<div class="spreadsheet-message">' + currentUserName + " : " + value + '</div>');
    }
}

webrtc.on('videoAdded', function (video, peer) {
    console.log('video added', peer);
    var remotes = (peer.nick.type === "former") ? document.getElementById('formerVideo') : document.getElementById('remotes');
    if (remotes) {
        var d = document.createElement('div');
        d.className = 'videoContainer';
        d.id = 'container_' + webrtc.getDomId(peer);
        d.appendChild(video);

        var vol = document.createElement('div');
        vol.id = 'volume_' + peer.id;
        vol.className = 'volume_bar';
        d.appendChild(vol);
        remotes.appendChild(d);

        //additional
        var actionsContent = document.createElement('div');
        actionsContent.id = 'user-actions_' + peer.id;
        actionsContent.className = 'user-actions';
        remotes.appendChild(actionsContent);

        var nameDiv = document.createElement('div');
        var newContent = document.createTextNode(peer.nick.name);
        nameDiv.appendChild(newContent);
        nameDiv.className = 'current-user-name';
        actionsContent.appendChild(nameDiv);

        var actionsDiv = document.createElement('div');
        var newContent = document.createTextNode(peer.nick.type);
        actionsDiv.appendChild(newContent);
        actionsDiv.className = 'current-user-actions';
        actionsContent.appendChild(actionsDiv);
    }
});
webrtc.on('videoRemoved', function (video, peer) {
    console.log('video removed ', peer);
    var remotes = document.getElementById('remotes');
    var el = document.getElementById('container_' + webrtc.getDomId(peer));
    var el2 = document.getElementById('user-actions_' + webrtc.getDomId(peer));
    if (remotes && el) {
        remotes.removeChild(el);
        el2.remove();
    }
});
webrtc.on('volumeChange', function (volume, treshold) {
    //console.log('own volume', volume);
    showVolume(document.getElementById('localVolume'), volume);
});

// Since we use this twice we put it here
function setRoom(name) {
    $('form').remove();
    $('h1').text(name);
    $('#subTitle').text('Link to join: ' + location.href);
    $('body').addClass('active');
}

if (room) {
    setRoom(room);
}

navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.getUserMedia;
     $('#screenShareButton').click(function() {
           navigator.getUserMedia({
                   audio: false,
                   video: {
                       mandatory: {
                            chromeMediaSource: 'desktop',
                            maxWidth: 1920,
                            maxHeight: 1080,
                            maxFrameRate: 10,
                            minAspectRatio: 1.77,
                            //chromeMediaSourceId: sourceId 
                       },
                       optional: []
                   }
               }, function(stream) {
                   
                   document.getElementById('localVideo').srcObject = stream;
//                    peer.getLocalStreams().forEach(function(stream) {
//                        peer.removeStream(stream);
//                    });
//                    peer.addStream(stream);
//                    peer.onnegotiationneeded = function() {
//                        if(peer.localDescription.type === 'offer') {
//                             renegotiatePeerConnect(); // renegotiate here
//                        }
//                     };
                   $('#screenShareButton').hide();
               }, function() {
                   alert('Error!');
                 }
           )
     });
     
     
     
// function renegotiate(newStream, isCreateOffer) {
//     peer.getRemoteStreams().forEach(function(remoteStream) {
//          peer.removeStream(remoteStream);
//     });
//
//     peer.addStream(newStream);
//     if(isCretaOffer) peer.createOffer(onSdpCallback, onErrorCallback, SdpAttributes);
//     else peer.cretaeAnswer(onSdpCallback, onErrorCallback, SdpAttributes);
//}