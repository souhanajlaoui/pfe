var currentEventData = {};
$(document).ready(function () {

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,listMonth,agendaWeek,listWeek,agendaDay,listDay'
        },
        views: {
            listMonth: { buttonText: 'List' },
            listWeek: { buttonText: 'List' },
            listDay: { buttonText: 'List' }
        },
        minTime: "08:00:00",
        maxTime: "20:00:00",
        defaultDate: stringToday(),
        navLinks: true, // can click day/week names to navigate views
        selectable: true,
        selectHelper: true,
        editable: true,
        eventRender: function(event, element, view) {
            $(".fc-list-item").html('<td clospan="3">' + event.html + '</td>');
            element.html(event.html);
        },
        eventResize: function(event, delta, revertFunc) {
            updateDatesSeance(event.id, event.start.format(), event.end.format());
        },
        eventDrop: function(event, delta, revertFunc) {
            updateDatesSeance(event.id, event.start.format(), event.end.format());
        },
        select: function (start, end) {
            currentEventData = {
                start: start,
                end: end
            };
            var startFormat = stringDatetime(start);
            var endFormat = stringDatetime(end);
            getSeanceForm(startFormat, endFormat);
        },
        eventLimit: true, // allow "more" link when too many events
        events: events
    });

});

/*
 * ----------------------------------------------------------------------------
 * AJAX SUBMIT FUNCTIONS
 *-----------------------------------------------------------------------------
 */

function getSeanceForm(startFormat, endFormat) {
    $('#form-modal').modal('show');
    $('#form-modal').on('hide.bs.modal', function (e) {
        $('#form-modal input').val('');
        $('#form-modal textarea').val('');
    });

    $('#seance_startAt').val(startFormat);
    $('#seance_endAt').val(endFormat);
}

function showSeance(element, title) {
    var url = element.attr('url');
    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        success: function (data) {
            $('body').append(data.html);
            if (title) {
                $('#form-modal-show .modal-title').html(title);
            }
            $('#form-modal-show').modal('show');
            $('#form-modal-show').on('hide.bs.modal', function (e) {
                $(this).remove();
            });
        },
        error: function (response) {
            notifyError();
        }
    });
}

function addSeance(form) {
    var url = form.attr('action');
    var data = form.serialize();
    initSubmit();
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: "json",
        success: function (data) {
            $('#form-modal').modal('hide');
            
            currentEventData.id = data.id;
            currentEventData.title = data.title;
            currentEventData.html = data.html;
            
            $('#calendar').fullCalendar('renderEvent', currentEventData, true); // stick? = true
            
            $('#calendar').fullCalendar('unselect');
            if (data.success) {
                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            } else {
                appendError(data.form);
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function updateDatesSeance(id, start, end) {
    
    $.ajax({
        url: Routing.generate('admin_seance_edit_date'),
        type: "POST",
        data: {id: id, start: start, end: end},
        dataType: "json",
        success: function (data) {
            if (data.success) {
                var prefix = (data.prefix) ? data.prefix:'seance';
                $('#' + prefix + '-' + data.id).html(data.html);
                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            } else {
                notifyError();
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function updateSeance(form) {
    var url = form.attr('action');
    var data = form.serialize();
    initSubmit();
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: "json",
        success: function (data) {
            if (data.success) {
                var prefix = (data.prefix) ? data.prefix:'seance';
                $('#' + prefix + '-' + data.id).html(data.html);
                $('#form-modal-edit').modal('hide');
                $('#form-modal-edit').remove();
                
                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            } else {
                appendError(data.form);
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function deleteSeance(element, title, body) {
    var url = element.attr('url');
    swal({
        title: (title) ? title : Translator.trans('modal.delete.title', {}, 'messages'),
        text: (body) ? body : Translator.trans('modal.delete.body', {}, 'messages'),
        buttons: {
            cancel: Translator.trans('modal.delete.cancel', {}, 'messages'),
            catch : {
                text: Translator.trans('modal.delete.confirm', {}, 'messages'),
                value: "delete"
            }
        },
        dangerMode: true
    })
    .then((value) => {
        if (value === 'delete') {
            confirmDeleteSeance(url);
        }
    });
}

function confirmDeleteSeance(url) {
    $.ajax({
        url: url,
        type: "DELETE",
        dataType: "json",
        success: function (data) {
            var prefix = (data.prefix) ? data.prefix:'item';
            $('#' + prefix + '-' + data.id).remove();
            $.notify({
                title: Translator.trans('modal.delete.success', {}, 'messages'),
                message: '',
                icon: 'fa fa-check'
            }, {
                type: "success"
            });
        },
        error: function (response) {
            notifyError();
        }
    });
}


/*
 * ----------------------------------------------------------------------------
 * DATE FUNCTIONS
 *-----------------------------------------------------------------------------
 */
function stringToday() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    dd = (dd < 10) ? '0' + dd : dd;
    mm = (mm < 10) ? '0' + mm : mm;
    
    return yyyy + '-' + mm + '-' + dd;
}

function stringDatetime(time) {
    var date = new Date(time);
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    dd = (dd < 10) ? '0' + dd : dd;
    mm = (mm < 10) ? '0' + mm : mm;
    
    var hh = date.getHours() - 1;
    var min = date.getMinutes();
    var ss = date.getSeconds();
    hh = (hh < 10) ? '0' + hh : hh;
    min = (min < 10) ? '0' + min : min;
    ss = (ss < 10) ? '0' + ss : ss;
    
    return yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min;
}


