var currentEventData = {};
$(document).ready(function () {

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,listMonth,agendaWeek,listWeek,agendaDay,listDay'
        },
        views: {
            listMonth: { buttonText: 'List' },
            listWeek: { buttonText: 'List' },
            listDay: { buttonText: 'List' }
        },
        minTime: "08:00:00",
        maxTime: "20:00:00",
        defaultDate: stringToday(),
        navLinks: true, // can click day/week names to navigate views
        selectable: true,
        selectHelper: true,
        editable: false,
        eventRender: function(event, element, view) {
            $(".fc-list-item").html('<td clospan="3">' + event.html + '</td>');
            element.html(event.html);
        },
        eventLimit: true, // allow "more" link when too many events
        events: events
    });

});

/*
 * ----------------------------------------------------------------------------
 * AJAX SUBMIT FUNCTIONS
 *-----------------------------------------------------------------------------
 */
function showSeance(element, title) {
    var url = element.attr('url');
    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        success: function (data) {
            $('body').append(data.html);
            if (title) {
                $('#form-modal-show .modal-title').html(title);
            }
            $('#form-modal-show').modal('show');
            $('#form-modal-show').on('hide.bs.modal', function (e) {
                $(this).remove();
            });
        },
        error: function (response) {
            notifyError();
        }
    });
}

/*
 * ----------------------------------------------------------------------------
 * DATE FUNCTIONS
 *-----------------------------------------------------------------------------
 */
function stringToday() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    dd = (dd < 10) ? '0' + dd : dd;
    mm = (mm < 10) ? '0' + mm : mm;
    
    return yyyy + '-' + mm + '-' + dd;
}