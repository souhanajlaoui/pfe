/**
 * Created by Roman Belousov on 07.04.16.
 */
$(document).ready(function () {
    setInterval(function () {
        if (ajax) {
            ajax.abort();
        }
        getMessage();
    }, 3000);
    var ajax;
    $('.belousovSendMessage').click(function () {
        var message = $('.belousovMessageText');
        var addressee = $('#belousovChat').attr('data-addressee');
        var url = $('form[name=chat]').attr('action');
        if (addressee == "" || message == "") {
            alert('change dialog');
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {'messageText': message.val(), 'addressee': addressee},
            cache: "false",
            dataType: "json",
            success: function (response) {
                if (response.error === undefined) {
                    message.val('');
                }
            }
        });
    });
    function getMessage() {
        var chat = $('#belousovChat');
        var user_id = chat.attr('data-author');
        var addressee = chat.attr('data-addressee');
        var url = chat.attr('data-action');
        
        if (addressee != '') {
            ajax = $.ajax({
                type: 'POST',
                url: url,
                data: {'user_id': user_id, 'addressee_id': addressee},
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    var resp = data.body;
                    var user = data.user;
                    if (resp.error === undefined) {
                        var html;
                        resp.forEach(function (item) {
                            if (item.author.id == user_id) {
                                html = appendMyMessage(item, user);
                                $('#belousovMessageZone').append(html);
                            }
                            if (item.addressee.id == user_id) {
                                html = appendUserMessage(item, user);
                                $('#belousovMessageZone').append(html);
                            }
                        });
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {'user_id': user_id, 'addressee_id': addressee, 'update': true},
                            dataType: 'json',
                            success: function (resp) {
                            }
                        });
                    }
                    setTimeout(function () {
                        getMessage()
                    }, 1000)
                }
            });
        }
    }
    $('.changeUser').click(function () {
        var addressee = $(this).attr('data-number');
        var chat = $('#belousovChat');
        var url = chat.attr('data-action');
        var user_id = chat.attr('data-author');
        chat.attr('data-addressee', addressee);
        $.ajax({
            type: "POST",
            url: url,
            data: {'changeUser': true, 'user_id': addressee},
            cache: "false",
            dataType: "json",
            success: function (data) {
                var response = data.body;
                var user = data.user;
                var html;
                
                $('#user-name').html(user.name);
                $('#belousovMessageZone').html('');
                if (ajax) {
                    ajax.abort();
                }
                getMessage();
                if (response) {
                    response.forEach(function (item) {
                        if (item.author.id == user_id) {
                            html = appendMyMessage(item, user);
                            $('#belousovMessageZone').append(html);
                        }
                        if (item.addressee.id == user_id) {
                            html = appendUserMessage(item, user);
                            $('#belousovMessageZone').append(html);
                        }
                    });
                }
            }
        });
    });
});


function appendUserMessage(item, user) {
    return '<div class="m-b">' +
                '<a href="" class="pull-left w-40 m-r-sm">' +
                  '<img src="/uploads/users/' + user.path + '" alt="..." class="w-full img-circle">' +
                '</a>' +
                '<div class="clear">' +
                  '<div class="m-t-xs">' +
                      '<div class="p-a p-y-sm dark-white inline r">' +
                        item.messageText +
                      '</div>' +
                  '</div>' +
                  '<div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i>' +
                  '2 minutes ago' +
                  '</div>' +
                '</div>' +
              '</div>';
}

function appendMyMessage(item, user) {
    return '<div class="m-b">' +
                '<a href="" class="pull-right w-40 m-l-sm">' +
                  '<img src="/uploads/users/' + user.path + '" alt="..." class="w-full img-circle">' +
                '</a>' +
                '<div class="clear text-right">' +
                  '<div class="m-t-xs">' +
                      '<div class="p-a p-y-sm info inline text-left r">' +
                        item.messageText +
                      '</div>' +
                  '</div>' +
                  '<div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i>' +
                  '2 minutes ago' +
                  '</div>' +
                '</div>' +
              '</div>';
}