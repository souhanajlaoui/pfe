var websocket = WS.connect("ws://127.0.0.1:1337");

websocket.on("socket/connect", function (session) {
    console.log(session)
    session.subscribe("chat/channel", function (uri, payload) {
        console.log("Received message", payload.msg);
    });

    session.call("sample/sum", {"term1": 2, "term2": 5}).then(
            function (result)
            {
                console.log("RPC Valid!", result);
            },
            function (error, desc)
            {
                console.log("RPC Error", error, desc);
            }
    );


    jQuery('#send').click(function () {
        var message = jQuery('#message').val();
        jQuery('#message').val('');
        session.publish("chat/channel", {msg: message});
    });

    jQuery("#message").keyup(function (e) {
        var code = e.which;
        if (code == 13) {
            var message = jQuery('#message').val();
            jQuery('#message').val('');
            session.publish("chat/channel", {msg: message});
        }
    });

    session.publish("chat/channel", {msg: "This is a message!"});

    session.publish("chat/channel", {msg: "I'm leaving, I will not see the next message"});

    session.unsubscribe("chat/channel");

    session.publish("chat/channel", {msg: "I won't see this"});

    session.subscribe("chat/channel", function (uri, payload) {
        console.log("Received message", payload.msg);
    });
    session.publish("chat/channel", {msg: "I'm back!"});
    /**/
});

websocket.on("socket/disconnect", function (error) {
    //error provides us with some insight into the disconnection: error.reason and error.code

    console.log("Disconnected for " + error.reason + " with code " + error.code);
});