/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ----------------------------------------------------------------------------
 * GLOBAL
 *-----------------------------------------------------------------------------
 */
$(document).ready(function(){
  
  /* Set active item */
  var current = $('a[href="' + location.pathname + '"]');
  current.addClass('active');
  $('.select2').select2();
  $('input[type="datetime-local"]').attr('type','text');
});

Pace.options = {ajax: true};
$(document).ajaxStart(function () {
//    Pace.restart();
//    $('body').append('<div id="ajax-fade" class="modal-backdrop fade in"></div>');
});

$(document).ajaxComplete(function () {
//    $('#ajax-fade').remove();
});

function notifyError() {
    $('#form-modal').modal('hide');
    $('#form-modal').remove();
    $.notify({
            title: Translator.trans('global.error', {}, 'messages'),
            message: '',
            icon: 'fa fa-warning'
        }, 
        {
            type: "danger"
        }
    );
}

/*
 * ----------------------------------------------------------------------------
 * AJAX SUBMIT FUNCTIONS
 *-----------------------------------------------------------------------------
 */

function getForm(element, title) {
    var url = element.attr('url');
    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        success: function (data) {
            $('body').append(data.html);
            if (title) {
                $('#form-modal .modal-title').html(title);
            }
            $('#form-modal').modal('show');
            $('#form-modal').on('hide.bs.modal', function (e) {
                $(this).remove();
            });
            $('.select2').select2();
        },
        error: function (response) {
            notifyError();
        }
    });
}

function addItem(form) {
    var url = form.attr('action');
    var data = form.serialize();
    initSubmit();
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: "json",
        success: function (data) {
            if (data.success) {
                var area = (data.prefix) ? data.prefix:'items';
                $('#' + area).prepend(data.html);
                $('#form-modal').modal('hide');
                $('#form-modal').remove();

                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            } else {
                appendError(data.form);
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function updateItem(form) {
    var url = form.attr('action');
    var data = form.serialize();
    initSubmit();
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: "json",
        success: function (data) {
            if (data.success) {
                var prefix = (data.prefix) ? data.prefix:'item';
                $('#' + prefix + '-' + data.id).html(data.html);
                $('#form-modal').modal('hide');
                $('#form-modal').remove();
                
                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            } else {
                appendError(data.form);
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function deleteItem(element, title, body) {
    var url = element.attr('url');
    swal({
        title: (title) ? title : Translator.trans('modal.delete.title', {}, 'messages'),
        text: (body) ? body : Translator.trans('modal.delete.body', {}, 'messages'),
        buttons: {
            cancel: Translator.trans('modal.delete.cancel', {}, 'messages'),
            catch : {
                text: Translator.trans('modal.delete.confirm', {}, 'messages'),
                value: "delete"
            }
        },
        dangerMode: true
    })
    .then((value) => {
        if (value === 'delete') {
            confirmDelete(url);
        }
    });
}

function confirmDelete(url) {
    $.ajax({
        url: url,
        type: "DELETE",
        dataType: "json",
        success: function (data) {
            var prefix = (data.prefix) ? data.prefix:'item';
            $('#' + prefix + '-' + data.id).remove();
            $.notify({
                title: Translator.trans('modal.delete.success', {}, 'messages'),
                message: '',
                icon: 'fa fa-check'
            }, {
                type: "success"
            });
        },
        error: function (response) {
            notifyError();
        }
    });
}


function addItemMultipart(form) {
    var url = $(form).attr('action');
    initSubmit();
    $.ajax({
        url: url,
        type: "POST",
        data: new FormData( form ),
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (data) {
            if (data.success) {
                var area = (data.prefix) ? data.prefix:'items';
                $('#' + area).prepend(data.html);
                $('#form-modal').modal('hide');
                $('#form-modal').remove();
                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            } else {
                appendError(data.form);
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function updateItemMultipart(form) {
    var url = $(form).attr('action');
    initSubmit();
    $.ajax({
        url: url,
        type: "POST",
        data: new FormData( form ),
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (data) {
            if (data.success) {
                var prefix = (data.prefix) ? data.prefix:'item';
                $('#' + prefix + '-' + data.id).html(data.html);
                $('#form-modal').modal('hide');
                $('#form-modal').remove();
                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            } else {
                appendError(data.form);
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function initSubmit() {
    $('.error-message').remove();
    $('input.has-error').removeClass('has-error');
}

function appendError(form) {
    $.each( form, function( key, value ) {
        var element = $('input[name="' + key +  '"]');
        element.addClass('has-error');
        element.parent().append('<span class="error-message">' + value[0] + '</span>');
    });
}

function enableUser(element, url) {
    var enabled = element.is(":checked");
    $.ajax({
        url: url,
        type: "POST",
        data: {enabled: enabled},
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function enableReclamation(element, url) {
    var status = element.is(":checked") ? 1:0;
    $.ajax({
        url: url,
        type: "POST",
        data: {status: status},
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $.notify({
                    title: Translator.trans('global.success', {}, 'messages'),
                    message: '',
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            }
        },
        error: function (response) {
            notifyError();
        }
    });
}

function showAttachment(element) {
    var url = element.attr('url');
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        success: function (data) {
            $('#file-preview').html(data.html);
        },
        error: function (response) {
            notifyError();
        }
    });
}