<?php

namespace Fides\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserEditType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('firstname', null, array(
                    'label' => 'user.firstname',
                    'translation_domain' => 'FidesUserBundle'
                ))
                ->add('lastname', null, array(
                    'label' => 'user.lastname',
                    'translation_domain' => 'FidesUserBundle'
                ))
                ->add('birthday', DateType::Class, array(
                    'label' => 'user.birthday',
                    'translation_domain' => 'FidesUserBundle',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'input-date'
                    ),
                    'years' => range(date('Y') - 60, date('Y') - 18),
                ))
                
                ->add('address', null, array(
                    'label' => 'user.address',
                    'translation_domain' => 'FidesUserBundle'
                ))
                ->add('mobile', null, array(
                    'label' => 'user.mobile',
                    'translation_domain' => 'FidesUserBundle',
                    'attr' => array(
                        'class' => 'input-number'
                    )
                ))
                ->add('cin', null, array(
                    'label' => 'user.cin',
                    'translation_domain' => 'FidesUserBundle',
                    'attr' => array(
                        'class' => 'input-number'
                    )
                ))
                ->add('nationality', null, array(
                    'label' => 'user.nationality',
                    'translation_domain' => 'FidesUserBundle',
                    'attr' => array(
                        'class' => 'input-number'
                    )
                ))
                ->add('image', null, array(
                    'label' => 'user.photo',
                    'translation_domain' => 'FidesUserBundle'
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Fides\UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'user';
    }

}
