<?php

namespace Fides\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('firstname', null, array(
                    'label' => 'user.firstname',
                    'translation_domain' => 'FidesUserBundle'
                ))
                ->add('lastname', null, array(
                    'label' => 'user.lastname',
                    'translation_domain' => 'FidesUserBundle'
                ))
                ->add('birthday', DateType::Class, array(
                    'label' => 'user.birthday',
                    'translation_domain' => 'FidesUserBundle',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'input-date'
                    ),
                    'years' => range(date('Y') - 60, date('Y') - 18),
                ))
                ->add('roles', CollectionType::class, array(
                    'label' => 'user.role',
                    'translation_domain' => 'FidesUserBundle',
                    'entry_type' => ChoiceType::class,
                    'entry_options' => array(
                        'label' => false,
                        'choices' => array(
                            'Formateur' => 'ROLE_FORMER',
                            'Étudiant' => 'ROLE_STUDENT',
                            'Administrateur' => 'ROLE_SUPER_ADMIN'
                        )
                    ),
                    'attr' => array(
                        'class' => 'col-sm-8'
                    )
                ))
                ->add('email', EmailType::class, array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
                ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
                ->add('plainPassword', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'options' => array(
                        'translation_domain' => 'FOSUserBundle',
                        'attr' => array(
                            'autocomplete' => 'new-password',
                        ),
                    ),
                    'first_options' => array('label' => 'form.password'),
                    'second_options' => array('label' => 'form.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
                ->add('address', null, array(
                    'label' => 'user.address',
                    'translation_domain' => 'FidesUserBundle'
                ))
                ->add('mobile', null, array(
                    'label' => 'user.mobile',
                    'translation_domain' => 'FidesUserBundle',
                    'attr' => array(
                        'class' => 'input-number'
                    )
                ))
                ->add('cin', null, array(
                    'label' => 'user.cin',
                    'translation_domain' => 'FidesUserBundle',
                    'attr' => array(
                        'class' => 'input-number'
                    )
                ))
                ->add('nationality', null, array(
                    'label' => 'user.nationality',
                    'translation_domain' => 'FidesUserBundle',
                    'attr' => array(
                        'class' => 'input-number'
                    )
                ))
                ->add('image', null, array(
                    'label' => 'user.photo',
                    'translation_domain' => 'FidesUserBundle'
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Fides\UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'user';
    }

}
