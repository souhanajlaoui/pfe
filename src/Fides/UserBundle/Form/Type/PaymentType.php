<?php

namespace Fides\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PaymentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('session')
                ->add('date', DateType::Class, array(
                    'label' => 'payment.date',
                    'translation_domain' => 'FidesAdminBundle',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'input-date'
                    ),
                    'years' => range(date('Y') , date('Y') + 10),
                ))
                ->add('amount', null, array(
                    'label' => 'payment.amount',
                    'translation_domain' => 'FidesAdminBundle'
                ))
                ->add('comment', null, array(
                    'label' => 'payment.comment',
                    'translation_domain' => 'FidesAdminBundle'
                ))
                ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fides\UserBundle\Entity\Payment'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fides_userbundle_message';
    }


}
