<?php

namespace Fides\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FidesUserBundle:Default:index.html.twig');
    }
    
    public function settingsAction()
    {
        return $this->render('FidesUserBundle:Default:settings.html.twig');
    }
}
