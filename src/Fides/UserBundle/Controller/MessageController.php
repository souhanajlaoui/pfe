<?php

namespace Fides\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Message controller.
 *
 */
class MessageController extends Controller
{
    /**
     * Get number of unread messages.
     *
     */
    public function unreadMessagesAction()
    {
        $provider = $this->get('fos_message.provider');
        $unreadMessages = $provider->getNbUnreadMessages();
        
        return new Response($unreadMessages);
    }
}
