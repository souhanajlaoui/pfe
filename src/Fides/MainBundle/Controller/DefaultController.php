<?php
/**
 * The default controller.
 *
 * @package FidesMainBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
namespace Fides\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use belousovr\belousovChatBundle\Form\ChatType;

/**
 * The default controller.
 *
 * @package FidesMainBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
class DefaultController extends Controller
{
    /**
     * Home page.
     *
     */
    public function indexAction()
    {
        return $this->render('FidesMainBundle:Default:index.html.twig');
    }
    
    /**
     * Chat.
     *
     */
    public function chatAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('FidesUserBundle:User')->findAll();

        return $this->render('chat.html.twig', array(
            'users' => $users, 
            'currentUser' => $this->getUser()
        ));
    }
    
    /**
     * Calendar page.
     *
     */
    public function calendarAction()
    {
        return $this->render('FidesMainBundle:Default:calendar.html.twig');
    }
    
    /**
     * Formation page.
     *
     */
    public function formationAction()
    {
        $em = $this->getDoctrine()->getManager();

        $formations = $em->getRepository('FidesMainBundle:Formation')->findAll();

        return $this->render('FidesMainBundle:Default:formation.html.twig', array(
            'formations' => $formations,
        ));
    }
}
