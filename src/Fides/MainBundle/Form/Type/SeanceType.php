<?php

namespace Fides\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class SeanceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('startAt', DateTimeType::Class, array(
                    'label' => 'seance.startAt',
                    'translation_domain' => 'FidesAdminBundle',
                    'widget' => 'single_text',
                    'years' => range(date('Y') , date('Y') + 10),
                ))
                ->add('endAt', DateTimeType::Class, array(
                    'label' => 'seance.endAt',
                    'translation_domain' => 'FidesAdminBundle',
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'input-date'
                    ),
                    'years' => range(date('Y') , date('Y') + 10),
                ))
                ->add('title', null, array(
                    'label' => 'seance.title',
                    'translation_domain' => 'FidesAdminBundle'
                ))
                ->add('description', null, array(
                    'label' => 'seance.description',
                    'translation_domain' => 'FidesAdminBundle'
                ))
                ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fides\MainBundle\Entity\Seance'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'seance';
    }


}
