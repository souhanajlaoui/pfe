<?php

namespace Fides\MainBundle\Form\Type;
;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\EntityRepository;

class SessionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('formation')
                ->add('former', EntityType::class, array(
                    'class' => 'FidesUserBundle:User',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->where('u.roles LIKE :roles')
                                ->setParameter('roles', '%"ROLE_FORMER"%');
                        }
                ))
                ->add('startAt', DateType::Class, array(
                    'label' => 'session.startAt',
                    'translation_domain' => 'FidesAdminBundle',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'input-date'
                    ),
                    'years' => range(date('Y') , date('Y') + 10),
                ))
                ->add('endAt', DateType::Class, array(
                    'label' => 'session.endAt',
                    'translation_domain' => 'FidesAdminBundle',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'input-date'
                    ),
                    'years' => range(date('Y') , date('Y') + 10),
                ))
                ->add('title')
                ->add('description')
                ->add('users')
                ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fides\MainBundle\Entity\Session'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'session';
    }


}
