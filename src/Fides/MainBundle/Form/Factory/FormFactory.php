<?php

namespace Fides\MainBundle\Form\Factory;

use Symfony\Component\Form\FormFactory as BaseFormFactory;
use Symfony\Component\Form\FormTypeInterface;

class FormFactory extends BaseFormFactory 
{

    public function createNamed($name, $type = 'Symfony\Component\Form\Extension\Core\Type\FormType', $data = null, array $options = array())
    {
        if (!is_string($type) && $type instanceof FormTypeInterface) {
          $type = get_class($type);
        }
        return $this->createNamedBuilder($name, $type, $data, $options)->getForm();
    }

    public function createNamedBuilder($name, $type = 'Symfony\Component\Form\Extension\Core\Type\FormType', $data = null, array $options = array()) 
    {
        if (!is_string($type) && $type instanceof FormTypeInterface) {
          $type = get_class($type);
        }
        return parent::createNamedBuilder($name, $type, $data, $options);
    }

}
