<?php

/**
 * Form Handler
 * 
 * @package NordnetUserBundle
 * @author Amine Fattouch
 */

namespace Fides\MainBundle\Form\Handler;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Fides\MainBundle\Entity\Course;

class CourseHandler
{

    /**
     * @var FormInterface
     */
    protected $form;
    
    /**
     * Constructor class.
     * 
     * @param FormInterface $form Instance of the form.
     */
    public function __construct(FormInterface $form)
    {
        $this->form = $form;
    }
    
    /**
     * The submit process edit of subvention.
     * 
     * @param Subvention $subvention The model of the current form.
     * @param Request    $request    The current request.
     * 
     * @return boolean
     */
    public function process(Request $request)
    {
        $process  = false;
        $course = new Course();
        
        $this->form->setData($course);
        if ('POST' == $request->getMethod())
        {
            $this->form->handleRequest($request);
//            if ($this->form->isValid())
//            {
                $process = true;
//            }
        }
        
        return $process;
    }
}
