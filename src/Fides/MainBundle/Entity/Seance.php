<?php
/**
 * The entity for Seance.
 *
 * @package FidesMainBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
namespace Fides\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * The entity for Seance.
 *
 * @package FidesMainBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 * 
 * @ORM\Entity
 * @ORM\Table(name="seance")
 */
class Seance
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    protected $title;
    
    /**
     * @ORM\Column(name="token", type="string", nullable=true)
     */
    protected $token;
    
    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var Formation
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $session;
    
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="start_at", type="datetime", nullable=true)
     */
    protected $startAt;
    
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="end_at", type="datetime", nullable=true)
     */
    protected $endAt;

    /*
     * toString
     * 
     * @return string
     */
    public function __toString() 
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Seance
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Seance
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     *
     * @return Seance
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     *
     * @return Seance
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set session
     *
     * @param \Fides\MainBundle\Entity\Session $session
     *
     * @return Seance
     */
    public function setSession(\Fides\MainBundle\Entity\Session $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return \Fides\MainBundle\Entity\Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set token.
     *
     * @param string|null $token
     *
     * @return Seance
     */
    public function setToken($token = null)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token.
     *
     * @return string|null
     */
    public function getToken()
    {
        return $this->token;
    }
}
