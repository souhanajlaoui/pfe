<?php
/**
 * The entity for course.
 *
 * @package FidesMainBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
namespace Fides\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * The entity for course.
 *
 * @package FidesMainBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 * 
 * @ORM\Entity(repositoryClass="Fides\MainBundle\Repository\CourseRepository")
 * @ORM\Table(name="course")
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Fides\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id")
     * })
     */
    protected $user;
    
    /**
     * @ORM\Column(name="title", type="string")
     */
    protected $title;
    
    /**
     * @ORM\Column(name="description", type="text")
     */
    protected $description;
    
    /**
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Attachment", mappedBy="course", cascade={"persist", "remove"})
     */
    protected $attachments;
    
    /**
     * @var Session
     *
     * @ORM\ManyToOne(targetEntity="Session", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $session;

    /**
     * @var \Datetime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;
    
    /**
     * @var \Datetime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attachments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Course
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Course
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Course
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Course
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add attachment
     *
     * @param \Fides\MainBundle\Entity\Attachment $attachment
     *
     * @return Course
     */
    public function addAttachment(\Fides\MainBundle\Entity\Attachment $attachment)
    {
        $attachment->setCourse($this);
        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Fides\MainBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Fides\MainBundle\Entity\Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set session
     *
     * @param \Fides\MainBundle\Entity\Session $session
     *
     * @return Course
     */
    public function setSession(\Fides\MainBundle\Entity\Session $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return \Fides\MainBundle\Entity\Session
     */
    public function getSession()
    {
        return $this->session;
    }
    
    /**
     * Get formation
     *
     * @return \Fides\MainBundle\Entity\Formation
     */
    public function getFormation()
    {
        return $this->session->getFormation();
    }

    /**
     * Set user
     *
     * @param \Fides\UserBundle\Entity\User $user
     *
     * @return Course
     */
    public function setUser(\Fides\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Fides\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
