<?php
/**
 * The entity for Session.
 *
 * @package FidesMainBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
namespace Fides\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * The entity for Session.
 *
 * @package FidesMainBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 * 
 * @ORM\Entity
 * @ORM\Table(name="session")
 */
class Session
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="title", type="string")
     */
    protected $title;
    
    /**
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Fides\UserBundle\Entity\User", inversedBy="formations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="former", referencedColumnName="id")
     * })
     */
    protected $former;
    
    /**
     * @var Formation
     *
     * @ORM\ManyToOne(targetEntity="Formation", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="formation", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $formation;
    
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="start_at", type="datetime", nullable=true)
     */
    protected $startAt;
    
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="end_at", type="datetime", nullable=true)
     */
    protected $endAt;
    
    /**
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Seance", mappedBy="session")
     */
    protected $seances;

    /**
     * @ORM\ManyToMany(targetEntity="Fides\UserBundle\Entity\User", inversedBy="sessions")
     * @ORM\JoinTable(name="users_sessions",
     *   joinColumns={@ORM\JoinColumn(name="user", referencedColumnName="id", onDelete="CASCADE")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="session", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $users;
    
    /**
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Course", mappedBy="session")
     */
    protected $courses;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seances = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->courses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Session
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Session
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     *
     * @return Session
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     *
     * @return Session
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set former
     *
     * @param \Fides\UserBundle\Entity\User $former
     *
     * @return Session
     */
    public function setFormer(\Fides\UserBundle\Entity\User $former = null)
    {
        $this->former = $former;

        return $this;
    }

    /**
     * Get former
     *
     * @return \Fides\UserBundle\Entity\User
     */
    public function getFormer()
    {
        return $this->former;
    }

    /**
     * Set formation
     *
     * @param \Fides\MainBundle\Entity\Formation $formation
     *
     * @return Session
     */
    public function setFormation(\Fides\MainBundle\Entity\Formation $formation = null)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return \Fides\MainBundle\Entity\Formation
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Add seance
     *
     * @param \Fides\MainBundle\Entity\Seance $seance
     *
     * @return Session
     */
    public function addSeance(\Fides\MainBundle\Entity\Seance $seance)
    {
        $this->seances[] = $seance;

        return $this;
    }

    /**
     * Remove seance
     *
     * @param \Fides\MainBundle\Entity\Seance $seance
     */
    public function removeSeance(\Fides\MainBundle\Entity\Seance $seance)
    {
        $this->seances->removeElement($seance);
    }

    /**
     * Get seances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeances()
    {
        return $this->seances;
    }

    /**
     * Add user
     *
     * @param \Fides\UserBundle\Entity\User $user
     *
     * @return Session
     */
    public function addUser(\Fides\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Fides\UserBundle\Entity\User $user
     */
    public function removeUser(\Fides\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
    
    /**
     * Add course
     *
     * @param \Fides\MainBundle\Entity\Course $course
     *
     * @return Formation
     */
    public function addCourse(\Fides\MainBundle\Entity\Course $course)
    {
        $this->courses[] = $course;

        return $this;
    }

    /**
     * Remove course
     *
     * @param \Fides\MainBundle\Entity\Course $course
     */
    public function removeCourse(\Fides\MainBundle\Entity\Course $course)
    {
        $this->courses->removeElement($course);
    }

    /**
     * Get courses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourses()
    {
        return $this->courses;
    }
    
    /*
     * toString
     * 
     * @return string
     */
    public function __toString() 
    {
        return '[' . $this->startAt->format('m-d-Y') . ' : ' . $this->endAt->format('m-d-Y') . '] ' . 
                 $this->formation . ' | ' .$this->title;
    }
    
    /*
     * get Name
     * 
     * @return string
     */
    public function getName() 
    {
        return $this->formation . ' | ' .$this->title;
    }
}
