<?php

/**
 * The main twig ewtension.
 *
 * @package FidesMainBundle
 */

namespace Fides\MainBundle\Twig;

/**
 * The main twig ewtension.
 *
 * @package FidesMainBundle
 */
class MainTwigExtension extends \Twig_Extension 
{
    /*
     * Retrun extension name.
     * 
     * @return string The extension name.
     */

    public function getName() 
    {
        return 'fides_twig_ext';
    }

    /*
     * Returns all extension functions.
     * 
     * @return array Array of functions.
     */

    public function getFunctions() 
    {
        return array(
            //'roles' => new \Twig_Function($this, 'roles', array('is_safe' => array('html')))
            new \Twig_Function('roles', array($this, 'roles'), array('is_safe' => array('html')))
        );
    }

    /*
     * Returns all extension filters.
     * 
     * @return array Array of filters.
     */

    public function getFilters() 
    {
        return array(
            new \Twig_SimpleFilter('count_unread_notifications', array($this, 'notifications')),
            new \Twig_SimpleFilter('role', array($this, 'role')),
            new \Twig_SimpleFilter('diff_date', array($this, 'diffDate')),
        );
    }

    /*
     * Return user role.
     * 
     * @param mixed $roles Collection of roles.
     * 
     * @return string Role.
     * 
     */

    function role($roles) 
    {
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $role = 'Administrateur';
        } elseif (in_array('ROLE_FORMER', $roles)) {
            $role = 'Formateur';
        } else {
            $role = 'Étudiant';
        }

        return $role;
    }

    /*
     * Count unread notifications notifications.
     * 
     * @param mixed $notifications Collection of notifications.
     * 
     * @return integer Number of unread notifications.
     * 
     */

    function notifications($notifications) 
    {
        $count = 0;
        foreach ($notifications as $notif) {
            if ($notif->getStatus() == 1) {
                $count++;
            }
        }

        return $count;
    }

    /*
     * Return difference between two dates.
     * 
     * @param Date $date Birthday.
     * 
     * @return integer Age.
     * 
     */

    function diffDate($date, $month = 0, $day = 0) 
    {
        $stringDiff = "";
        $today = new \DateTime('today');
        $diff = $date->diff($today);
        if ($diff->y > 0) {
            $stringDiff = ($diff->y === 1) ? $diff->y . " an" : $diff->y . " ans";
        } else {
            $month = 1;
        }

        if ($month) {
            $stringDiff = ($diff->y > 0) ? $stringDiff . " " : $stringDiff;
            $stringDiff .= $diff->m . " mois";
        }

        if ($day) {
            $stringDiff .= " " . $diff->d . " jour";
            $stringDiff = ($diff->d > 1) ? $stringDiff . "s" : $stringDiff;
        }

        return $stringDiff;
    }

}
