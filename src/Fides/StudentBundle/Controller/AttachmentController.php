<?php

namespace Fides\StudentBundle\Controller;

use Fides\MainBundle\Entity\Attachment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Attachment controller.
 *
 */
class AttachmentController extends Controller
{
    /**
     * Lists all attachment entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $attachments = $em->getRepository('FidesMainBundle:Attachment')->findAll();

        return $this->render('FidesAdminBundle:Attachment:index.html.twig', array(
            'attachments' => $attachments,
        ));
    }

    /**
     * Finds and displays a attachment entity.
     *
     */
    public function showAction(Attachment $attachment)
    {
        $html = $this->renderView('FidesAdminBundle:Attachment:pdf_preview.html.twig', array(
            'attachment' => $attachment,
        ));
            
        $response = array('success' => true, 'html' => $html);
        
        return new JsonResponse($response);
    }
}
