<?php

namespace Fides\StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user = $this->getUser();
        $sessions = $user->getSessions();
        return $this->render('FidesStudentBundle:Default:index.html.twig', array(
            'sessions' => $sessions
        ));
    }
}
