<?php

namespace Fides\StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Fides\MainBundle\Entity\Session;

/**
 * Session controller.
 *
 */
class SessionController extends Controller
{
    /**
     * Lists user session entities.
     *
     */
    public function indexAction()
    {
        return $this->render('FidesStudentBundle:Session:index.html.twig', array());
    }
    
    /**
     * Finds and displays a session entity.
     *
     */
    public function showAction(Session $session)
    {
        return $this->render('FidesStudentBundle:Session:show.html.twig', array(
            'session' => $session
        ));
    }
}
