<?php
/**
 * The Course controller.
 *
 * @package FidesStudentBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
namespace Fides\StudentBundle\Controller;

use Fides\MainBundle\Entity\Course;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * The Course controller.
 *
 * @package FidesStudentBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
class CourseController extends Controller
{
    /**
     * Lists user course entities.
     *
     */
    public function indexAction()
    {
        return $this->render('FidesStudentBundle:Course:index.html.twig', array());
    }
    
    /**
     * Finds and displays a course entity.
     *
     */
    public function showAction(Course $course)
    {
        return $this->render('FidesStudentBundle:Course:show.html.twig', array(
            'course' => $course,
        ));
    }
}
