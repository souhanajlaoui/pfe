<?php

namespace Fides\FormerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user = $this->getUser();
        $sessions = $user->getFormations();
        return $this->render('FidesFormerBundle:Default:index.html.twig', array(
            'sessions' => $sessions
        ));
    }
}
