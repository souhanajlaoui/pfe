<?php

namespace Fides\FormerBundle\Controller;

use Fides\MainBundle\Entity\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Session controller.
 *
 */
class SessionController extends Controller
{
    /**
     * Lists all session entities.
     *
     */
    public function indexAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $sessions = $em->getRepository('FidesMainBundle:Session')->findBy(array('former' => $user->getId()));

        return $this->render('FidesFormerBundle:Session:index.html.twig', array(
            'sessions' => $sessions,
        ));
    }

    /**
     * Finds and displays a session entity.
     *
     */
    public function showAction(Session $session)
    {
        return $this->render('FidesFormerBundle:Session:show.html.twig', array(
            'session' => $session
        ));
    }
}
