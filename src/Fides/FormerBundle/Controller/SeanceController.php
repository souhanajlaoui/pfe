<?php

namespace Fides\FormerBundle\Controller;

use Fides\MainBundle\Entity\Seance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Seance controller.
 *
 */
class SeanceController extends Controller
{
    /**
     * Lists all seance entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $seances = $em->getRepository('FidesMainBundle:Seance')->findAll();

        return $this->render('FidesFormerBundle:Seance:index.html.twig', array(
            'seances' => $seances,
        ));
    }
    
    /**
     * Seance RT.
     *
     */
    public function seanceRtAction(Request $request, Seance $seance)
    {
        return $this->render('FidesFormerBundle:Seance:seance_rt.html.twig', array(
            'seance' => $seance,
        ));
    }
    
    /**
     * Finds and displays a seance entity.
     *
     */
    public function showAction(Seance $seance)
    {
        $html = $this->renderView('FidesFormerBundle:Seance:show.html.twig', array(
            'seance' => $seance,
        ));
            
        $response = array('success' => true, 'html' => $html);
        
        return new JsonResponse($response);
    }
    
    /**
     * Finds and displays a seance entity.
     *
     */
    public function showItemAction(Seance $seance)
    {
        $html = $this->renderView('FidesFormerBundle:Seance:item.html.twig', array(
            'seance' => $seance
        ));
        
        return new Response($html);
    }
}
