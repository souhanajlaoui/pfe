<?php

namespace Fides\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('FidesUserBundle:User')->findAll();
        $formations = $em->getRepository('FidesMainBundle:Formation')->findAll();
        $sessions = $em->getRepository('FidesMainBundle:Session')->findAll();
        
        return $this->render('FidesAdminBundle:Default:index.html.twig', array(
            'users' => $users,
            'formations' => $formations,
            'sessions' => $sessions
        ));
    }
    
    public function smsAction()
    {
        //returns an instance of Vresh\TwilioBundle\Service\TwilioWrapper
    	$twilio = $this->get('twilio.api');

        $message = $twilio->account->messages->sendMessage(
            '+12299999591', // From a Twilio number in your account
            '+21695249891', // Text any number
            "Hello User !"
        );

        return new Response($message->sid);
    }
}
