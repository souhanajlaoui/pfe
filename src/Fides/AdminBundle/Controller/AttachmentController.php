<?php

namespace Fides\AdminBundle\Controller;

use Fides\MainBundle\Entity\Attachment;
use Fides\MainBundle\Entity\Course;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Attachment controller.
 *
 */
class AttachmentController extends Controller
{
    /**
     * Lists all attachment entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $attachments = $em->getRepository('FidesMainBundle:Attachment')->findAll();

        return $this->render('FidesAdminBundle:Attachment:index.html.twig', array(
            'attachments' => $attachments,
        ));
    }

    /**
     * Creates a new form for attachment entity.
     *
     */
    public function newFormAction(Request $request, Course $course)
    {
        $attachment = new Attachment();
        $form = $this->createForm('Fides\MainBundle\Form\Type\AttachmentType', $attachment);
        $form->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Attachment:form.html.twig', array(
            'attachment' => $attachment,
            'course' => $course,
            'form' => $form->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Creates a new attachment entity.
     *
     */
    public function newAction(Request $request, Course $course)
    {
        $attachment = new Attachment();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm('Fides\MainBundle\Form\Type\AttachmentType', $attachment);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $attachment->setCourse($course);
            $em->persist($attachment);
            $em->flush();
            
            $html = $this->renderView('FidesAdminBundle:Attachment:item.html.twig', array(
                'attachment' => $attachment,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $attachment->getId(), 'prefix' => 'attachments');
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($form));
        }

        return new JsonResponse($response);
    }

    /**
     * Finds and displays a attachment entity.
     *
     */
    public function showAction(Attachment $attachment)
    {
        $html = $this->renderView('FidesAdminBundle:Attachment:pdf_preview.html.twig', array(
            'attachment' => $attachment,
        ));
            
        $response = array('success' => true, 'html' => $html);
        
        return new JsonResponse($response);
    }

    /**
     * Displays a form to edit an existing attachment entity.
     *
     */
    public function editFormAction(Request $request, Attachment $attachment)
    {
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\AttachmentType', $attachment);
        $editForm->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Attachment:form_edit.html.twig', array(
            'attachment' => $attachment,
            'form' => $editForm->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Displays a form to edit an existing attachment entity.
     *
     */
    public function editAction(Request $request, Attachment $attachment)
    {
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\AttachmentType', $attachment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            
            $html = $this->renderView('FidesAdminBundle:Attachment:sub_item.html.twig', array(
                'attachment' => $attachment,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $attachment->getId(), 'prefix' => 'attachment');
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($editForm));
        }

        return new JsonResponse($response);
    }

    /**
     * Deletes a attachment entity.
     *
     */
    public function deleteAction(Attachment $attachment)
    {
        $deletedId = $attachment->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($attachment);
        $em->flush();

        return new JsonResponse(array('id' => $deletedId, 'prefix' => 'attachment'));
    }

    /**
     * Creates a form to delete a attachment entity.
     *
     * @param Attachment $attachment The attachment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Attachment $attachment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('attachment_delete', array('id' => $attachment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    protected function getErrorMessages(\Symfony\Component\Form\Form $form) 
    {
        $errors = array();
        $formName = $form->getName();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }
        
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $key = ($formName !== '') ? $formName . '[' . $child->getName() . ']' : $child->getName(); 
                $errors[$key] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
