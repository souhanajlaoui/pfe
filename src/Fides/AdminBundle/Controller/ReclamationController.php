<?php

namespace Fides\AdminBundle\Controller;

use Fides\MainBundle\Entity\Reclamation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Reclamation controller.
 *
 */
class ReclamationController extends Controller
{
    /**
     * Lists all reclamation entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reclamations = $em->getRepository('FidesMainBundle:Reclamation')->findAll();

        return $this->render('FidesAdminBundle:Reclamation:index.html.twig', array(
            'reclamations' => $reclamations,
        ));
    }

    /**
     * Creates a new reclamation entity.
     *
     */
    public function newAction(Request $request)
    {
        $reclamation = new Reclamation();
        $form = $this->createForm('Fides\MainBundle\Form\Type\ReclamationType', $reclamation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $reclamation->setUser($this->getUser());
            $em->persist($reclamation);
            $em->flush();

            return $this->redirectToRoute('main_reclamation_show', array('id' => $reclamation->getId()));
        }

        return $this->render('FidesAdminBundle:Reclamation:new.html.twig', array(
            'reclamation' => $reclamation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a reclamation entity.
     *
     */
    public function showAction(Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);

        return $this->render('FidesAdminBundle:Reclamation:show.html.twig', array(
            'reclamation' => $reclamation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reclamation entity.
     *
     */
    public function editAction(Request $request, Reclamation $reclamation)
    {
        $status = (boolean) $request->get('status');
        $em = $this->getDoctrine()->getManager();
        $reclamation->setStatus($status);
        $em->persist($reclamation);
        $em->flush();
            
        $response = array('success' => true, 'id' => $reclamation->getId());
       
        return new JsonResponse($response);
    }

    /**
     * Deletes a reclamation entity.
     *
     */
    public function deleteAction(Reclamation $reclamation)
    {
        $deletedId = $reclamation->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($reclamation);
        $em->flush();

        return new JsonResponse(array('id' => $deletedId));
    }

    /**
     * Creates a form to delete a reclamation entity.
     *
     * @param Reclamation $reclamation The reclamation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reclamation $reclamation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('main_reclamation_delete', array('id' => $reclamation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
