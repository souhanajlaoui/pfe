<?php

namespace Fides\AdminBundle\Controller;

use Fides\UserBundle\Entity\Payment;
use Fides\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Payment controller.
 *
 */
class PaymentController extends Controller
{
    /**
     * Lists all payment entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $payments = $em->getRepository('FidesMainBundle:Payment')->findAll();

        return $this->render('FidesAdminBundle:Payment:index.html.twig', array(
            'payments' => $payments,
        ));
    }

    /**
     * Creates a new form for payment entity.
     *
     */
    public function newFormAction(Request $request, User $user)
    {
        $payment = new Payment();
        $form = $this->createForm('Fides\UserBundle\Form\Type\PaymentType', $payment);
        $form->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Payment:form.html.twig', array(
            'payment' => $payment,
            'user' => $user,
            'form' => $form->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Creates a new payment entity.
     *
     */
    public function newAction(Request $request, User $user)
    {
        $payment = new Payment();
        $form = $this->createForm('Fides\UserBundle\Form\Type\PaymentType', $payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $payment->setUser($user);
            $em->persist($payment);
            $em->flush();
            
            $html = $this->renderView('FidesAdminBundle:Payment:item.html.twig', array(
                'payment' => $payment,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $payment->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($form));
        }
        
        return new JsonResponse($response);
    }

    /**
     * Finds and displays a payment entity.
     *
     */
    public function showAction(Payment $payment)
    {
        $deleteForm = $this->createDeleteForm($payment);

        return $this->render('FidesAdminBundle:Payment:show.html.twig', array(
            'payment' => $payment,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing payment entity.
     *
     */
    public function editFormAction(Request $request, Payment $payment)
    {
        $editForm = $this->createForm('Fides\UserBundle\Form\Type\PaymentType', $payment);
        $editForm->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Payment:form_edit.html.twig', array(
            'payment' => $payment,
            'edit_form' => $editForm->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Displays a form to edit an existing payment entity.
     *
     */
    public function editAction(Request $request, Payment $payment)
    {
        $editForm = $this->createForm('Fides\UserBundle\Form\Type\PaymentType', $payment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();        
            $html = $this->renderView('FidesAdminBundle:Payment:sub_item.html.twig', array(
                'payment' => $payment,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $payment->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($editForm));
        }

        return new JsonResponse($response);
    }

    /**
     * Deletes a payment entity.
     *
     */
    public function deleteAction(Payment $payment)
    {
        $deletedId = $payment->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($payment);
        $em->flush();

        return new JsonResponse(array('id' => $deletedId));
    }

    /**
     * Creates a form to delete a payment entity.
     *
     * @param Payment $payment The payment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Payment $payment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('payment_delete', array('id' => $payment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    protected function getErrorMessages(\Symfony\Component\Form\Form $form) 
    {
        $errors = array();
        $formName = $form->getName();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }
        
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $key = ($formName !== '') ? $formName . '[' . $child->getName() . ']' : $child->getName(); 
                $errors[$key] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
