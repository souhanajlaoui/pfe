<?php

namespace Fides\AdminBundle\Controller;

use Fides\MainBundle\Entity\Session;
use Fides\MainBundle\Entity\Seance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Session controller.
 *
 */
class SessionController extends Controller
{
    /**
     * Lists all session entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sessions = $em->getRepository('FidesMainBundle:Session')->findAll();

        return $this->render('FidesAdminBundle:Session:index.html.twig', array(
            'sessions' => $sessions,
        ));
    }

    /**
     * Creates a new form for session entity.
     *
     */
    public function newFormAction(Request $request)
    {
        $session = new Session();
        $form = $this->createForm('Fides\MainBundle\Form\Type\SessionType', $session);
        $form->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Session:form.html.twig', array(
            'session' => $session,
            'form' => $form->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Creates a new session entity.
     *
     */
    public function newAction(Request $request)
    {
        $session = new Session();
        $form = $this->createForm('Fides\MainBundle\Form\Type\SessionType', $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($session);
            $em->flush();
            
            $html = $this->renderView('FidesAdminBundle:Session:item.html.twig', array(
                'session' => $session,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $session->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($form));
        }
        
        return new JsonResponse($response);
    }

    /**
     * Finds and displays a session entity.
     *
     */
    public function showAction(Request $request, Session $session)
    {
        $seance = new Seance();
        $form = $this->createForm('Fides\MainBundle\Form\Type\SeanceType', $seance);
        $form->handleRequest($request);
        
        return $this->render('FidesAdminBundle:Session:show.html.twig', array(
            'session' => $session,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing session entity.
     *
     */
    public function editFormAction(Request $request, Session $session)
    {
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\SessionType', $session);
        $editForm->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Session:form_edit.html.twig', array(
            'session' => $session,
            'edit_form' => $editForm->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Displays a form to edit an existing session entity.
     *
     */
    public function editAction(Request $request, Session $session)
    {
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\SessionType', $session);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();        
            $html = $this->renderView('FidesAdminBundle:Session:sub_item.html.twig', array(
                'session' => $session,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $session->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($editForm));
        }

        return new JsonResponse($response);
    }

    /**
     * Deletes a session entity.
     *
     */
    public function deleteAction(Session $session)
    {
        $deletedId = $session->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($session);
        $em->flush();

        return new JsonResponse(array('id' => $deletedId));
    }

    /**
     * Creates a form to delete a session entity.
     *
     * @param Session $session The session entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Session $session)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('session_delete', array('id' => $session->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    protected function getErrorMessages(\Symfony\Component\Form\Form $form) 
    {
        $errors = array();
        $formName = $form->getName();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }
        
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $key = ($formName !== '') ? $formName . '[' . $child->getName() . ']' : $child->getName(); 
                $errors[$key] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
