<?php

namespace Fides\AdminBundle\Controller;

use Fides\MainBundle\Entity\Formation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Formation controller.
 *
 */
class FormationController extends Controller
{
    /**
     * Lists all formation entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $formations = $em->getRepository('FidesMainBundle:Formation')->findAll();

        return $this->render('FidesAdminBundle:Formation:index.html.twig', array(
            'formations' => $formations,
        ));
    }

    /**
     * Creates a new form for formation entity.
     *
     */
    public function newFormAction(Request $request)
    {
        $formation = new Formation();
        $form = $this->createForm('Fides\MainBundle\Form\Type\FormationType', $formation);
        $form->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Formation:form.html.twig', array(
            'formation' => $formation,
            'form' => $form->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Creates a new formation entity.
     *
     */
    public function newAction(Request $request)
    {
        $formation = new Formation();
        $form = $this->createForm('Fides\MainBundle\Form\Type\FormationType', $formation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();
            
            $html = $this->renderView('FidesAdminBundle:Formation:item.html.twig', array(
                'formation' => $formation,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $formation->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($form));
        }
        
        return new JsonResponse($response);
    }

    /**
     * Finds and displays a formation entity.
     *
     */
    public function showAction(Formation $formation)
    {
        return $this->render('FidesAdminBundle:Formation:show.html.twig', array(
            'formation' => $formation,
        ));
    }

    /**
     * Displays a form to edit an existing formation entity.
     *
     */
    public function editFormAction(Request $request, Formation $formation)
    {
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\FormationType', $formation);
        $editForm->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Formation:form_edit.html.twig', array(
            'formation' => $formation,
            'edit_form' => $editForm->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Displays a form to edit an existing formation entity.
     *
     */
    public function editAction(Request $request, Formation $formation)
    {
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\FormationType', $formation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();        
            $html = $this->renderView('FidesAdminBundle:Formation:sub_item.html.twig', array(
                'formation' => $formation,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $formation->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($editForm));
        }

        return new JsonResponse($response);
    }

    /**
     * Deletes a formation entity.
     *
     */
    public function deleteAction(Formation $formation)
    {
        $deletedId = $formation->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($formation);
        $em->flush();

        return new JsonResponse(array('id' => $deletedId));
    }

    /**
     * Creates a form to delete a formation entity.
     *
     * @param Formation $formation The formation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Formation $formation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('formation_delete', array('id' => $formation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    protected function getErrorMessages(\Symfony\Component\Form\Form $form) 
    {
        $errors = array();
        $formName = $form->getName();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }
        
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $key = ($formName !== '') ? $formName . '[' . $child->getName() . ']' : $child->getName(); 
                $errors[$key] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
