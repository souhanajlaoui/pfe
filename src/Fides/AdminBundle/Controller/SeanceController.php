<?php

namespace Fides\AdminBundle\Controller;

use Fides\MainBundle\Entity\Seance;
use Fides\MainBundle\Entity\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Seance controller.
 *
 */
class SeanceController extends Controller
{
    /**
     * Lists all seance entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $seances = $em->getRepository('FidesMainBundle:Seance')->findAll();

        return $this->render('FidesAdminBundle:Seance:index.html.twig', array(
            'seances' => $seances,
        ));
    }
    
    /**
     * Creates a new form for seance entity.
     *
     */
    public function newFormAction(Request $request, Session $session)
    {
        $seance = new Seance();
        $form = $this->createForm('Fides\MainBundle\Form\Type\SeanceType', $seance);
        $form->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Seance:form.html.twig', array(
            'seance' => $seance,
            'session' => $session,
            'form' => $form->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Creates a new seance entity.
     *
     */
    public function newAction(Request $request, Session $session)
    {
        $seance = new Seance();
        $form = $this->createForm('Fides\MainBundle\Form\Type\SeanceType', $seance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $seance->setSession($session);
            $seance->setToken(uniqid());
            $em->persist($seance);
            $em->flush();
            
            $html = $this->renderView('FidesAdminBundle:Seance:item.html.twig', array(
                'seance' => $seance,
            ));
            
            $response = array(
                'success' => true, 
                'id' => $seance->getId(), 
                'start' => $seance->getStartAt()->format('Y-m-d\\TH:i:s'), 
                'end' => $seance->getEndAt()->format('Y-m-d\\TH:i:s'), 
                'title' => $seance->getTitle(),
                'html' => $html
            );
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($form));
        }
        
        return new JsonResponse($response);
    }

    /**
     * Finds and displays a seance entity.
     *
     */
    public function showAction(Seance $seance)
    {
        $html = $this->renderView('FidesAdminBundle:Seance:show.html.twig', array(
            'seance' => $seance,
        ));
            
        $response = array('success' => true, 'html' => $html);
        
        return new JsonResponse($response);
    }
    
    /**
     * Finds and displays a seance entity.
     *
     */
    public function showItemAction(Seance $seance)
    {
        $html = $this->renderView('FidesAdminBundle:Seance:item.html.twig', array(
            'seance' => $seance
        ));
        
        return new Response($html);
    }

    /**
     * Displays a form to edit an existing seance entity.
     *
     */
    public function editFormAction(Request $request, Seance $seance)
    {
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\SeanceType', $seance);
        $editForm->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:Seance:form_edit.html.twig', array(
            'seance' => $seance,
            'form' => $editForm->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Displays a form to edit an existing seance entity.
     *
     */
    public function editAction(Request $request, Seance $seance)
    {
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\SeanceType', $seance);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();        
            $html = $this->renderView('FidesAdminBundle:Seance:sub_item.html.twig', array(
                'seance' => $seance,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $seance->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($editForm));
        }

        return new JsonResponse($response);
    }
    
    /**
     * Displays a form to edit an existing seance entity.
     *
     */
    public function editDatesAction(Request $request)
    {
        $id = $request->get('id');
        $start = new \Datetime($request->get('start'));
        $end = new \Datetime($request->get('end'));
        
        $em = $this->getDoctrine()->getManager();
        $seance = $em->getRepository('FidesMainBundle:Seance')->findOneById($id);

        $seance->setStartAt($start);
        $seance->setEndAt($end);
        $em->persist($seance);
        $em->flush();

        $html = $this->renderView('FidesAdminBundle:Seance:sub_item.html.twig', array(
            'seance' => $seance,
        ));
            
        $response = array('success' => true, 'html' => $html, 'id' => $id);
       
        return new JsonResponse($response);
    }

    /**
     * Deletes a seance entity.
     *
     */
    public function deleteAction(Seance $seance)
    {
        $deletedId = $seance->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($seance);
        $em->flush();

        return new JsonResponse(array('id' => $deletedId));
    }

    /**
     * Creates a form to delete a seance entity.
     *
     * @param Seance $seance The seance entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Seance $seance)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('seance_delete', array('id' => $seance->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    protected function getErrorMessages(\Symfony\Component\Form\Form $form) 
    {
        $errors = array();
        $formName = $form->getName();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }
        
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $key = ($formName !== '') ? $formName . '[' . $child->getName() . ']' : $child->getName(); 
                $errors[$key] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
