<?php
/**
 * The Course controller.
 *
 * @package FidesAdminBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
namespace Fides\AdminBundle\Controller;

use Fides\MainBundle\Entity\Course;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * The Course controller.
 *
 * @package FidesAdminBundle
 * @author  Souha Najlaoui
 * @author  Omar benzarti
 */
class CourseController extends Controller
{
    /**
     * Lists all course entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $courses = $em->getRepository('FidesMainBundle:Course')->findAll();

        return $this->render('FidesAdminBundle:Course:index.html.twig', array(
            'courses' => $courses,
        ));
    }

    /**
     * Creates a new course entity.
     *
     */
    public function newAction(Request $request)
    {
        $course = new Course();
        $form = $this->createForm('Fides\MainBundle\Form\Type\CourseType', $course);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $course->setUser($this->getUser());
            $em->persist($course);
            $em->flush();

            return $this->redirectToRoute('admin_course_show', array('id' => $course->getId()));
        }

        return $this->render('FidesAdminBundle:Course:new.html.twig', array(
            'course' => $course,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a course entity.
     *
     */
    public function showAction(Course $course)
    {
        $deleteForm = $this->createDeleteForm($course);
        
        return $this->render('FidesAdminBundle:Course:show.html.twig', array(
            'course' => $course,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing course entity.
     *
     */
    public function editAction(Request $request, Course $course)
    {
        $deleteForm = $this->createDeleteForm($course);
        $editForm = $this->createForm('Fides\MainBundle\Form\Type\CourseType', $course);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('course_edit', array('id' => $course->getId()));
        }

        return $this->render('FidesAdminBundle:Course:edit.html.twig', array(
            'course' => $course,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a course entity.
     *
     */
    public function deleteAction(Course $course)
    {
        $deletedId = $course->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($course);
        $em->flush();
        
        return new JsonResponse(array('id' => $deletedId));
    }

    /**
     * Creates a form to delete a course entity.
     *
     * @param Course $course The course entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Course $course)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('course_delete', array('id' => $course->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
