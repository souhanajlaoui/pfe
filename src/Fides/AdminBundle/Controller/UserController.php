<?php

namespace Fides\AdminBundle\Controller;

use Fides\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('FidesUserBundle:User')->findAll();

        return $this->render('FidesAdminBundle:User:index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Creates a new form for user entity.
     *
     */
    public function newFormAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('Fides\UserBundle\Form\Type\UserType', $user);
        $form->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:User:form.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Creates a new user entity.
     *
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('Fides\UserBundle\Form\Type\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();
            
            $html = $this->renderView('FidesAdminBundle:User:item.html.twig', array(
                'user' => $user,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $user->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($form));
        }

        return new JsonResponse($response);
    }

    /**
     * Finds and displays a user entity.
     *
     */
    public function showAction(User $user)
    {
        return $this->render('FidesAdminBundle:User:show.html.twig', array(
            'user' => $user
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function editFormAction(Request $request, User $user)
    {
        $editForm = $this->createForm('Fides\UserBundle\Form\Type\UserType', $user);
        $editForm->handleRequest($request);

        $html = $this->renderView('FidesAdminBundle:User:form_edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
        ));
        
        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm('Fides\UserBundle\Form\Type\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            
            $html = $this->renderView('FidesAdminBundle:User:sub_item.html.twig', array(
                'user' => $user,
            ));
            
            $response = array('success' => true, 'html' => $html, 'id' => $user->getId());
        } else {
            $response = array('success' => false, 'form' => $this->getErrorMessages($editForm));
        }

        return new JsonResponse($response);
    }

    /**
     * Deletes a user entity.
     *
     */
    public function deleteAction(User $user)
    {
        $deletedId = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return new JsonResponse(array('id' => $deletedId));
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    /**
     * Enable / disable user.
     *
     */
    public function enableAction(Request $request, User $user)
    {
        $enabled = $request->get('enabled');
        $em = $this->getDoctrine()->getManager();
        $user->setEnabled($enabled);
        $em->persist($user);
        $em->flush();
            
        $response = array('success' => true, 'id' => $user->getId());
       
        return new JsonResponse($response);
    }
    
    protected function getErrorMessages(\Symfony\Component\Form\Form $form) 
    {
        $errors = array();
        $formName = $form->getName();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }
        
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $key = ($formName !== '') ? $formName . '[' . $child->getName() . ']' : $child->getName(); 
                $errors[$key] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
