<?php

namespace Fides\ChatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FidesChatBundle:Default:index.html.twig');
    }
}
