<?php
/**
 * This file is part of the RatchetBundle project.
 *
 * (c) 2013 Philipp Boes <mostgreedy@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Fides\ChatBundle\WebSocket\Client;

use P2\Bundle\RatchetBundle\WebSocket\Client\ClientProviderInterface;

/**
 * Interface ClientProviderInterface
 * @package P2\Bundle\RatchetBundle\WebSocket\Client
 */
class ClientProvider implements ClientProviderInterface
{
    /**
     * Returns a client found by the access token.
     *
     * @param string $accessToken
     *
     * @return ClientInterface
     */
    public function findByAccessToken($accessToken) 
    {
        
    }

    /**
     * Updates the given client in the underlying data layer.
     *
     * @param ClientInterface $client
     * @return void
     */
    public function updateClient(ClientInterface $client)
    {
        
    }
}
